import { createApp } from 'vue'
import App from './App.vue'
import VueAgile from 'vue-agile'

createApp(App)
  .use(VueAgile)

createApp(App).mount('#app')
